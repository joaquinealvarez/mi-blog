$(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
      interval: 1000
    });

    $('#subscribite').on('show.bs.modal', function(e){
      console.log('El modal se esta mostrando');
      $('#subscribiteBtn').removeClass('btn-danger');
      $('#subscribiteBtn').addClass('btn-primary');
      $('#subscribiteBtn').prop('disabled', true);
    })
    $('#subscribite').on('shown.bs.modal', function(e){
      console.log('El modal se mostro');
    })
    $('#subscribite').on('hide.bs.modal', function(e){
      console.log('El modal se esta ocultando');
      $('#subscribiteBtn').removeClass('btn-primary');
      $('#subscribiteBtn').addClass('btn-danger');
      $('#subscribiteBtn').prop('disabled', false);
    })
    $('#subscribite').on('hidden.bs.modal', function(e){
      console.log('El modal se oculto');
    })
      });